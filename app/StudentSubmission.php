<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentSubmission extends Model
{
    protected $fillable = ['student_id', 'question_id', 'mark_id', 'answer'];
}
