<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MultipleQuestion extends Model
{
    protected $fillable = ['name','question_id'];
}
