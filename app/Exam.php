<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $fillable = ['exam_type_id', 'subject_id', 'duration', 'teacher_id'];
}
