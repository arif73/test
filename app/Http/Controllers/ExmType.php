<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExamType;
use DB;
use Illuminate\Support\Facades\Redirect;


class ExmType extends Controller
{


    public function index(){

        return view('admin.addexam');
    }

    public function postExamType(Request $request){

        $this->validate($request, [
            'name' => 'required'

        ]);

        $exam = new Examtype();
        $exam->name = $request->input('name');

        $exam->save();

//        return Redirect::back()->with('message', 'Data save successfully');
        return redirect();

    }
}
